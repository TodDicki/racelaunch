﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour {

	float currentTime = 0;

	[SerializeField]
	Text timeDisplay;

	float seconds = 0;
	int minutes = 0;


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		currentTime += Time.deltaTime;

		seconds = currentTime % 60;

		minutes = (int)(currentTime / 60);


		timeDisplay.text = minutes + "\' " + (int)seconds + "\'\'" + ((seconds % 1) * 1000).ToString("F0");

	}
}
