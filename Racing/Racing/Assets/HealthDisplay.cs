﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthDisplay : MonoBehaviour {

	[SerializeField]
	Health healthToDisplay;
	[SerializeField]
	Color lowHealthColor;
	[SerializeField]
	Color midHealthColor;
	[SerializeField]
	Color maxHealthColor;

	[SerializeField]
	Text healthText;

	// Use this for initialization
	void Awake () {
		healthToDisplay.OnHealthChanged.AddListener (() => ChangeHealthDisplay ());
	}
	
	void ChangeHealthDisplay()
	{
		float healthPercent = healthToDisplay.CurrentHealth / healthToDisplay.maxHealth;
		if(healthPercent < .25f)
		{
			healthText.color = lowHealthColor;
		}
		else if(healthPercent < .75f)
		{
			healthText.color = midHealthColor;
		}
		else if(healthPercent < 1)
		{
			healthText.color = maxHealthColor;
		}

		healthText.text = "Health: " + (healthPercent * 100) + "%";

	}
}
