﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIController : MonoBehaviour {

	public Waypoint currentWaypoint;

	Transform tf;
	Mover move;

	Vector3 direction = Vector3.zero;



	// Use this for initialization
	void Start () {
		tf = GetComponent<Transform> ();
		move = GetComponent<Mover> ();
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 tempDir = new Vector3 ();
		direction = currentWaypoint.transform.position - tf.position;
		Debug.DrawLine (tf.position, currentWaypoint.transform.position, Color.red);
//		tempDir.x = -direction.z;
//		tempDir.z = direction.x;
//		direction = tempDir;
		direction = tf.InverseTransformDirection (direction);


		//direction.y = 0;
		direction.Normalize ();
		Debug.Log ("AI: " + direction);
	}

	void FixedUpdate()
	{
		move.Move (direction);

	}
}
