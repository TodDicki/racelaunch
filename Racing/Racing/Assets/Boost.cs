﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boost : MonoBehaviour {

	[SerializeField]
	float speedBoost = 5;

	[SerializeField]
	float increaseTimer = 2;

	[SerializeField]
	float boostDecaySpeed = 5;

	void OnTriggerEnter(Collider other)
	{
		if (other.GetComponent<Mover> ()) {
			other.GetComponent<Mover> ().isBoosting = true;
			other.GetComponent<Mover> ().tempSpeed += speedBoost;
			StartCoroutine(BoostTimer(other.GetComponent<Mover>()));
		}

	}

//	void OnTriggerExit(Collider other)
//	{
//		if (other.GetComponent<Mover> ()) {
//			other.GetComponent<Mover> ().isBoosting = false;
//			other.GetComponent<Mover> ().ResetSpeed ();
//		}
//
//	}

	IEnumerator BoostTimer(Mover move)
	{
		float t = 0;
		while (t < increaseTimer) {
			t += Time.deltaTime;
			yield return null;
		}
		StartCoroutine (move.ResetSpeed(boostDecaySpeed));
	}


}
