﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


public class Health : MonoBehaviour {

	public float maxHealth = 100;

	float currentHealth = 100;

	[HideInInspector]
	public Transform lastCheckpoint;


	public UnityEvent OnHealthChanged;
	public UnityEvent OnDeath;

	public float CurrentHealth
	{
		get{ return currentHealth; }
		set
		{ 
			currentHealth = value;
			OnHealthChanged.Invoke ();
			if (currentHealth <= 0) {
				OnDeath.Invoke ();
				
			}
		}

	}

	// Use this for initialization
	void Start () {
		currentHealth = maxHealth;
		OnDeath.AddListener (() => StartCoroutine (Respawn()));
	}

	void OnCollisionEnter(Collision other)
	{
		if (other.gameObject.GetComponent<Ground> ())
			return;

		CurrentHealth -= 5;
	}

	
	IEnumerator Respawn()
	{
		GetComponent<AnimationHandler> ().PlayAnimation ("Explosion");
		float t = 0;
		while (t < 1) {
			t += Time.deltaTime;
			yield return null;
		}
		currentHealth = maxHealth;
		GetComponent<Mover> ().tempSpeed = 0;
		transform.position = lastCheckpoint.position;
		transform.rotation = lastCheckpoint.rotation;

	}
}
