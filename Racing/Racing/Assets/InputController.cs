﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputController : MonoBehaviour {

	Vector3 direction = Vector3.zero;

	Mover move;



	// Use this for initialization
	void Start () {
		move = GetComponent<Mover> ();

	}

	// Update is called once per frame
	void Update () {
		direction.x = Input.GetAxis ("Horizontal");
		direction.z = Input.GetAxis ("Vertical");
	


		direction.Normalize ();

		Debug.Log ("Player: " + direction);
	}

	void FixedUpdate()
	{
		move.Move (direction);
	}
}
