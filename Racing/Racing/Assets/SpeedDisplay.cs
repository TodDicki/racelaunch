﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpeedDisplay : MonoBehaviour {

	[SerializeField]
	Text speedDisplay;
	[SerializeField]
	Mover move;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		speedDisplay.text = (int)move.displaySpeed + " mph";
	}
}
