﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mover : MonoBehaviour {

	[SerializeField]
	float maxSpeed = 25;

	float speed = 5;
	[SerializeField]
	float turnSpeed = 3;

	[HideInInspector]
	public float tempSpeed;
	[HideInInspector]
	public bool isBoosting;

	public float displaySpeed;

	AnimationHandler handler;
	Transform tf;
	[SerializeField]
	Animator[] engineTrailAnimators;

	void Start()
	{
		handler = GetComponent<AnimationHandler> ();
		tf = GetComponent<Transform> ();
		tempSpeed =  maxSpeed * .5f;
	}


	public void Move(Vector3 direction)
	{
		if (direction.z > 0) {
			tempSpeed = Mathf.Lerp (tempSpeed, maxSpeed, Time.deltaTime);
		} else {
			tempSpeed = Mathf.Lerp (tempSpeed, 0, Time.deltaTime);
		
		}
		for (int i = 0; i < engineTrailAnimators.Length; i++) {
			engineTrailAnimators[i].speed = tempSpeed * .2f;
		}

		displaySpeed = tempSpeed / Time.deltaTime * .35f;

		tf.position += tf.forward * tempSpeed * Time.deltaTime;

		tf.Rotate (Vector3.up, direction.x * turnSpeed * Time.deltaTime, Space.Self);;

		if (!isBoosting) {
			if (direction.x < 0) {
				handler.PlayAnimation ("LeftTurn");
			} else if (direction.x > 0) {
				handler.PlayAnimation ("RightTurn");
			} else {
				handler.PlayAnimation ("Default");
			}
		} else if (isBoosting) {
			if (direction.x < 0) {
				handler.PlayAnimation ("BoostLeft");
			} else if (direction.x > 0) {
				handler.PlayAnimation ("BoostRight");
			} else {
				handler.PlayAnimation ("BoostDefault");
			}
		}
	}

	public IEnumerator ResetSpeed(float decay)
	{
		float t = 0;
		while (t < 1) {
			t += decay * Time.deltaTime;
			tempSpeed = Mathf.Lerp (tempSpeed, speed, t * Time.deltaTime);
			yield return null;
		}
		tempSpeed = speed;
		isBoosting = false;
	}
}
