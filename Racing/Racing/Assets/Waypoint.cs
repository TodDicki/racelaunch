﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Waypoint : MonoBehaviour {

	[SerializeField]
	Waypoint nextWaypoint;

	void OnTriggerEnter(Collider other)
	{
		if (other.GetComponent<AIController> ()) {
			other.GetComponent<AIController> ().currentWaypoint = nextWaypoint;
			
		}
		if (other.GetComponent<Health> ()) {
			other.GetComponent<Health> ().lastCheckpoint = this.transform;
		}

	}
	
}
