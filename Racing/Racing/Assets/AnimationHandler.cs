﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationHandler : MonoBehaviour {

	[SerializeField]
	Animator anim;

	void Start()
	{
		if (!anim) {
			anim = GetComponent<Animator> ();
		}


	}

	public void PlayAnimation(string animName)
	{
		anim.Play (animName);
	}
}
